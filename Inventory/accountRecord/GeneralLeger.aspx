﻿<%@ Page Title="" Language="C#" MasterPageFile="~/navBar.Master" AutoEventWireup="true" CodeBehind="Generalleger.aspx.cs" Inherits="Inventory.WebForm11" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Content/css/tranStyle.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      <div class="hero">
        <div class="form-box-large">
            <div class="form-box-top"><h2 style="text-align:center;margin:2px;">General Ledger Detail</h2></div>
            <div class="form-box-left">
                <fieldset class="columnbox">
               
        <legend style="color:goldenrod; font-size:15px">Select Data</legend>
                      <div class="each">
                          F:<asp:DropDownList ID="DropDownList3" runat="server" Width="40%">
										
                                    </asp:DropDownList>
                            To:<asp:DropDownList ID="DropDownList1" runat="server" Width="40%">
										
                                    </asp:DropDownList>
                          <br />

                          </div>
                  
                      <div class="each">
                          F:<asp:DropDownList ID="DropDownList2" runat="server" Width="40%">
										
                                    </asp:DropDownList>
                            To:<asp:DropDownList ID="DropDownList4" runat="server" Width="40%">
										
                                    </asp:DropDownList>
                          <br />

                          </div>
                </fieldset>

                <br />
                <fieldset class="columnbox">
               
        <legend style="color:coral; font-size:15px;">Select Project Type</legend>
                    <div class="each">
                <asp:RadioButton ID="RadioButton1" Text="All Projects" runat="server" GroupName="project_Type" />
        </div> 
                      <div class="each">
                <asp:RadioButton ID="RadioButton2" Text="Particular Projects" runat="server" GroupName="project_Type" />
                 </div> 
                </fieldset>
                  <fieldset class="columnbox" style="height:20%;">
               
        <legend style="font-display:block; font-size:15px;">Select Account Type</legend>
                    <div class="each">
                <asp:RadioButton ID="RadioButton3" Text="Child Account" runat="server" GroupName="acc_Type" /><br />
                        <asp:DropDownList ID="DropDownList6" runat="server" Width="90%"/>

        </div>   
                      <div class="each" style="margin-top:3%; margin-bottom:2%;">
                <asp:RadioButton ID="RadioButton4" Text="Particular Projects" runat="server" GroupName="acc_Type" /><br />
                          <asp:DropDownList ID="DropDownList5" runat="server" Width="90%"/>
                 </div> 
                </fieldset>
               
                <div class="largebtn">
                      <asp:Button ID="Button2" runat="server" Text="On Grid"/> 
                </div>
               
                 <div class="largebtn">
                      <asp:Button ID="Button1" runat="server" Text="Preview"/> 
                </div>
               <div class="largebtn">
                      <asp:Button ID="Button3" runat="server" Text="Close"/> 
                </div>
                 <div style="margin-top:1%;">
                     <fieldset class="columnbox">
               
        <legend style="color:coral; font-size:15px;">Order By</legend>
                    <div class="each">
                <asp:RadioButton ID="RadioButton5" Text="By Name" runat="server" GroupName="project_Type" />
        </div> 
                        
                      <div class="each">
                <asp:RadioButton ID="RadioButton6" Text="By Amount(Asc)" runat="server" GroupName="project_Type" />
                          
                 </div> 
                           <div class="each">
                <asp:RadioButton ID="RadioButton7" Text="By Amount(Desc)" runat="server" GroupName="project_Type" />
                          
                 </div> 
                      <br />    
                </fieldset>
                </div>
            </div>
            <div class="form-box-right">
                
            </div>

        </div>
    </div>
</asp:Content>
