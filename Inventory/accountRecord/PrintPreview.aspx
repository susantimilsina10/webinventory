﻿<%@ Page Title="" Language="C#" MasterPageFile="~/navBar.Master" AutoEventWireup="true" CodeBehind="PrintPreview.aspx.cs" Inherits="Inventory.WebForm8" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Content/css/tranStyle.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="hero">
        <div class="form-box-large">
            <div class="form-box-top"><h2 style="text-align:center;margin:2px;">Print Preview Vouchers </h2></div>
            <div class="form-box-left">
                <fieldset class="columnbox">
               
        <legend style="color:goldenrod; font-size:15px">Select Data</legend>
                      <div class="each">
                          F:<asp:DropDownList ID="D3" runat="server" Width="40%">
										<asp:ListItem Text="Volvo"></asp:ListItem>
										<asp:ListItem Text="Mercedes"></asp:ListItem>
										<asp:ListItem Text="Audi"></asp:ListItem>
										<asp:ListItem Text="Saab"></asp:ListItem>
                                    </asp:DropDownList>
                            To:<asp:DropDownList ID="D1" runat="server" Width="40%">
										<asp:ListItem Text="Volvo"></asp:ListItem>
										<asp:ListItem Text="Mercedes"></asp:ListItem>
										<asp:ListItem Text="Audi"></asp:ListItem>
										<asp:ListItem Text="Saab"></asp:ListItem>
                                    </asp:DropDownList>
                          <br />

                          </div>
                      <div class="each">
                          F:<asp:DropDownList ID="D2" runat="server" Width="40%">
										<asp:ListItem Text="Volvo"></asp:ListItem>
										<asp:ListItem Text="Mercedes"></asp:ListItem>
										<asp:ListItem Text="Audi"></asp:ListItem>
										<asp:ListItem Text="Saab"></asp:ListItem>
                                    </asp:DropDownList>
                            To:<asp:DropDownList ID="D4" runat="server" Width="40%">
										<asp:ListItem Text="Volvo"></asp:ListItem>
										<asp:ListItem Text="Mercedes"></asp:ListItem>
										<asp:ListItem Text="Audi"></asp:ListItem>
										<asp:ListItem Text="Saab"></asp:ListItem>
                                    </asp:DropDownList>
                          <br />

                          </div>
                </fieldset>

            <fieldset class="columnbox">
               
        <legend style="font-display:block; font-size:15px;">Select Voucher Type</legend>
                <div class="each">
                <asp:RadioButton ID="JV" Text="Journal Voucher" runat="server" GroupName="Voucher_Type" />
           </div>
                <div class="each">
                <asp:RadioButton ID="RV" Text="Receipt Voucher" runat="server" GroupName="Voucher_Type" />
                          </div>
                <div class="each">
                <asp:RadioButton ID="PayV" Text="Payment Voucher" runat="server" GroupName="Voucher_Type" />
                    </div>
                </fieldset>
                <fieldset class="columnbox">
               
        <legend style="font-display:block; font-size:15px;">Select Report Type</legend>
                    <div class="each">
                <asp:RadioButton ID="AllV" Text="All Vouchers" runat="server" GroupName="report_Type" />
           </div>
                      <div class="each">
                <asp:RadioButton ID="PV" Text="Particular Vouchers" runat="server" GroupName="report_Type" />
                 </div> 
                </fieldset>
                <fieldset class="columnbox">
               
        <legend style="color:coral; font-size:15px;">Select Project Type</legend>
                    <div class="each">
                <asp:RadioButton ID="R1" Text="All Projects" runat="server" GroupName="project_Type" />
        </div> 
                      <div class="each">
                <asp:RadioButton ID="R2" Text="Particular Projects" runat="server" GroupName="project_Type" />
                 </div> 
                </fieldset>
                <div class="largebtn">
                      <asp:Button ID="B2" runat="server" Text="Preview"/> 
                </div>
                 <div class="largebtn">
                      <asp:Button ID="B1" runat="server" Text="Preview"/> 
                </div>
               
                   <fieldset class="columnbox" style="">
               
        <legend style="color:coral; font-size:15px;">Select Order By</legend>
                    <div class="each">
                <asp:RadioButton ID="VR" Text="Voucher Ref" runat="server" GroupName="order_by" />
        </div> 
                      <div class="each">
                <asp:RadioButton ID="EN" Text="Event No" runat="server" GroupName="order_by" />
                 </div> 
                </fieldset>
            </div>
            <div class="form-box-right"></div>

        </div>
    </div>
</asp:Content>
