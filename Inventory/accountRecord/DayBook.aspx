﻿<%@ Page Title="" Language="C#" MasterPageFile="~/navBar.Master" AutoEventWireup="true" CodeBehind="DayBook.aspx.cs" Inherits="Inventory.WebForm10" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Content/css/tranStyle.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

        <div class="form-box-large">
            <div class="form-box-top"><h2 style="text-align:center;">Profit And Loss</h2></div>
            <div class="form-box-left">
                <fieldset class="columnbox">
               
        <legend style="color:goldenrod; font-size:15px">Select Data</legend>
                      <div class="each">
                          F:<asp:DropDownList ID="d1" runat="server" Width="40%">
										
                                    </asp:DropDownList>
                            To:<asp:DropDownList ID="d2" runat="server" Width="40%">
									
                                    </asp:DropDownList>
                          <br />

                          </div>
                  
                      <div class="each">
                          F:<asp:DropDownList ID="d3" runat="server" Width="40%">
										
                                    </asp:DropDownList>
                            To:<asp:DropDownList ID="d4" runat="server" Width="40%">
										
                                    </asp:DropDownList>
                          <br />

                          </div>
                </fieldset>

                <br />
                <fieldset class="columnbox">
               
        <legend style="color:coral; font-size:15px;">Select Project Type</legend>
                    <div class="each">
                <asp:RadioButton ID="r1" Text="All Projects" runat="server" GroupName="project_Type" />
        </div> 
                      <div class="each">
                <asp:RadioButton ID="r2" Text="Particular Projects" runat="server" GroupName="project_Type" />
                 </div> 
                </fieldset>
                <br />
                <div class="largebtn">
                      <asp:Button ID="B2" runat="server" Text="Preview"/> 
                </div>
                <br />
                 <div class="largebtn">
                      <asp:Button ID="B1" runat="server" Text="Close"/> 
                </div>
                <div style="margin-top:30%;">
                     <fieldset class="columnbox">
               
        <legend style="color:coral; font-size:15px;">Order By</legend>
                    <div class="each">
                <asp:RadioButton ID="RadioButton3" Text="By Name" runat="server" GroupName="project_Type" />
        </div> 
                        
                      <div class="each">
                <asp:RadioButton ID="RadioButton4" Text="By Amount(Asc)" runat="server" GroupName="project_Type" />
                          
                 </div> 
                           <div class="each">
                <asp:RadioButton ID="RadioButton5" Text="By Amount(Desc)" runat="server" GroupName="project_Type" />
                          
                 </div> 
                      <br />    
                </fieldset>
                </div>
               
                
            </div>
            <div class="form-box-right"></div>

        </div>
  
</asp:Content>
