﻿<%@ Page Title="" Language="C#" MasterPageFile="~/navBar.Master" AutoEventWireup="true" CodeBehind="ProfitNLoss.aspx.cs" Inherits="Inventory.WebForm9" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Content/css/tranStyle.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div class="hero">
        <div class="form-box-large">
            <div class="form-box-top"><h2 style="text-align:center;margin:2px;">Profit And Loss</h2></div>
            <div class="form-box-left">
                <fieldset class="columnbox">
               
        <legend style="color:goldenrod; font-size:15px">Select Data</legend>
                      <div class="each">
                          F:<asp:DropDownList ID="D3" runat="server" Width="40%">
										<asp:ListItem Text="Volvo"></asp:ListItem>
										<asp:ListItem Text="Mercedes"></asp:ListItem>
										<asp:ListItem Text="Audi"></asp:ListItem>
										<asp:ListItem Text="Saab"></asp:ListItem>
                                    </asp:DropDownList>
                            To:<asp:DropDownList ID="D1" runat="server" Width="40%">
										<asp:ListItem Text="Volvo"></asp:ListItem>
										<asp:ListItem Text="Mercedes"></asp:ListItem>
										<asp:ListItem Text="Audi"></asp:ListItem>
										<asp:ListItem Text="Saab"></asp:ListItem>
                                    </asp:DropDownList>
                          <br />

                          </div>
                  
                      <div class="each">
                          F:<asp:DropDownList ID="D2" runat="server" Width="40%">
										
                                    </asp:DropDownList>
                            To:<asp:DropDownList ID="D4" runat="server" Width="40%">
										
                                    </asp:DropDownList>
                          <br />

                          </div>
                </fieldset>

                <br />
                <fieldset class="columnbox">
               
        <legend style="color:coral; font-size:15px;">Select Project Type</legend>
                    <div class="each">
                <asp:RadioButton ID="R1" Text="All Projects" runat="server" GroupName="project_Type" />
        </div> 
                      <div class="each">
                <asp:RadioButton ID="R2" Text="Particular Projects" runat="server" GroupName="project_Type" />
                 </div> 
                </fieldset>
                <br />
                <div class="largebtn">
                      <asp:Button ID="B2" runat="server" Text="Preview"/> 
                </div>
                <br />
                 <div class="largebtn">
                      <asp:Button ID="B1" runat="server" Text="Cancel"/> 
                </div>
               
                
            </div>
            <div class="form-box-right"></div>

        </div>
    </div>
</asp:Content>
