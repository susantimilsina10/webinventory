﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Inventory
{
    public partial class WebForm9 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            gvDistricts.DataSource = dt.DefaultView;
            gvDistricts.DataBind();
            grandTotal.Style.Add("text-align", "right");
        }
    }
}