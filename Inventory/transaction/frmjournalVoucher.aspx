﻿<%@ Page Title="" Language="C#" MasterPageFile="~/navBar.Master" AutoEventWireup="true" CodeBehind="frmjournalVoucher.aspx.cs" Inherits="Inventory.WebForm1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
  
    <link href="../Content/css/icons.css" rel="stylesheet" />
	 <link href="../Content/css/tranStyle.css" rel="stylesheet" />
      <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link href="https://use.fontawesome.com/releases/v5.0.7/css/all.css" rel="stylesheet">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    
<script>
    $(function () {
        $(".datepicker").datepicker();
        $("#anim").on("change", function () {
            $(".datepicker").datepicker("option", "showAnim", $(this).val());
        });
    });
	</script>
    <style type="text/css">
        .FixedHeader {
            position: absolute;
            font-weight: bold;
        } 
        
        .labels{
            text-align:right
        }

              [type="date"] {
  background:#fff url(https://cdn1.iconfinder.com/data/icons/cc_mono_icon_set/blacks/16x16/calendar_2.png)  97% 50% no-repeat ;
}
[type="date"]::-webkit-inner-spin-button {
  display: none;
}
[type="date"]::-webkit-calendar-picker-indicator {
  opacity: 0;
}

    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

 <div class="hero">
		<div class="form-box">
            <br />
			<h2 align="center">Journal Voucher Entry - Event Wise</h2>
			<br/><br/>
			<table>
				<tr>
					<td style="text-align:center;"><asp:Label runat="server" Width="150" > Transaction Date :</asp:Label></td>
					<td>&nbsp;<asp:DropDownList ID="DropDownList3" runat="server" Width="150">
										<asp:ListItem Text="Volvo"></asp:ListItem>
										<asp:ListItem Text="Mercedes"></asp:ListItem>
										<asp:ListItem Text="Audi"></asp:ListItem>
										<asp:ListItem Text="Saab"></asp:ListItem>
                                    </asp:DropDownList>	</td>
					
					<td>
                        <input type="date"  name="dateofbirth"  value="<% DateTime.Now %>" id="dateofbirth" style="width:100px" runat="server">
                    </td>
						<td style="text-align:center;"><asp:Label runat="server" Width="155"  > Voucher No. :</asp:Label></td>
					<td>
						<asp:DropDownList ID="DropDownList4" runat="server" Width="150" style="margin-right:20px;" >
								<asp:ListItem Text="Volvo"></asp:ListItem>
								<asp:ListItem Text="Mercedes"></asp:ListItem>
								<asp:ListItem Text="Audi"></asp:ListItem>
								<asp:ListItem Text="Saab"></asp:ListItem>
                        </asp:DropDownList>	</td>
					<td >Doc No:</td>
					<td>&nbsp;
                  <asp:TextBox ID="TextBox2" runat="server" Width="150"></asp:TextBox></td>
				</tr>
			</table>
			<asp:Panel runat="server" ID="createPanel" HorizontalAlign="Right" >
				<asp:Button runat="server" Text="Create Ledger" Width="175px" CssClass="submit-btn" />
				&nbsp; &nbsp; 
         
			</asp:Panel>
             <br />
			<asp:Panel ID="groupPanel" runat="server" >
                    <div style="overflow-y: scroll; overflow-x: hidden; height: 300px; width: 100%;">
			 <asp:GridView ID="gvDistricts" runat="server" style="height:400px; overflow:scroll" 
                    HeaderStyle-CssClass="FixedHeader"
                    AutoGenerateColumns="false" AlternatingRowStyle-BackColor="WhiteSmoke" 
                    ShowHeaderWhenEmpty="True"> 
                             <AlternatingRowStyle BackColor="WhiteSmoke" />
                             <Columns>
                                 <asp:TemplateField HeaderStyle-Width="80px" HeaderStyle-Height="40px" HeaderText="Sr" ItemStyle-Width="80px">
                                     <ItemTemplate>
                                         <asp:Label ID="lblDistID" runat="server" Text='hi'></asp:Label>
                                     </ItemTemplate>
                                     <HeaderStyle Width="80px" />
                                     <ItemStyle Width="80px" />
                                 </asp:TemplateField>
                                 <asp:TemplateField HeaderText="Account Name">
                                     <ItemTemplate>
                                         <asp:Label ID="lblDistName" runat="server" Text='hlo'></asp:Label>
                                     </ItemTemplate>
                                     <HeaderStyle Width="250px" />
                                     <ItemStyle Width="250px" />
                                 </asp:TemplateField>
                                 <asp:TemplateField HeaderText="Particulars">
                                     <ItemTemplate>
                                         <asp:Label ID="lblDistDesc" runat="server" Text='hh'></asp:Label>
                                     </ItemTemplate>
                                     <HeaderStyle Width="300px" />
                                     <ItemStyle Width="300px" />
                                 </asp:TemplateField>
                                 <asp:TemplateField HeaderText="Debit">
                                     <ItemTemplate>
                                         <asp:Label ID="lblDistDesc" runat="server" Text='gg'></asp:Label>
                                     </ItemTemplate>
                                     <HeaderStyle Width="174px" />
                                     <ItemStyle Width="174px" />
                                 </asp:TemplateField>
                                 <asp:TemplateField HeaderText="Credit">
                                     <ItemTemplate>
                                         <asp:Label ID="lblDistDesc" runat="server" Text='bye'></asp:Label>
                                     </ItemTemplate>
                                     <HeaderStyle Width="174px" />
                                     <ItemStyle Width="174px" />
                                 </asp:TemplateField>                                                   
                             </Columns>
                             <HeaderStyle BackColor="YellowGreen" />
                    </asp:GridView>  
                        </div>
                </asp:Panel>
                       <asp:Table runat="server" BorderStyle="Solid" BorderWidth="1px" GridLines="Both" Font-Size="Small">
                           <asp:TableHeaderRow>
                               <asp:TableHeaderCell HorizontalAlign="Center" Width="630">Total Amount</asp:TableHeaderCell>
                               <asp:TableHeaderCell HorizontalAlign="Center" Width="175">Debit</asp:TableHeaderCell>
                               <asp:TableHeaderCell HorizontalAlign="Center" Width="175">Credit</asp:TableHeaderCell>
                           </asp:TableHeaderRow>
                           </asp:Table>
            <br />
            <asp:Table runat="server"  BorderWidth="0px" Font-Size="Small">
                           <asp:TableHeaderRow>
                               <asp:TableHeaderCell HorizontalAlign="Left" Width="630">&nbsp; &nbsp;Zero</asp:TableHeaderCell>
                               <asp:TableHeaderCell HorizontalAlign="Center" Width="174" >Difference : </asp:TableHeaderCell>
                               <asp:TableHeaderCell HorizontalAlign="Center" Width="174"><asp:TextBox runat="server" Width="150px"></asp:TextBox></asp:TableHeaderCell>
                           </asp:TableHeaderRow>
                           </asp:Table>                   
				<br />
               <button runat="server" id="Button1" class="frmBtn"><i class="fa fa-save fa-2x" style="color: Highlight; padding-right:10px;" ></i>Save</button>
               <button runat="server" id="Button2" class="frmBtn"><i class="fa fa-trash fa-2x" style="color: red; padding-right:10px;"></i>Delete </button>
               <button runat="server" id="Button3" class="frmBtn"><i class="fa fa-edit fa-2x" style="color: greenyellow; padding-right:10px;"></i>Modify </button>
               <button runat="server" id="Button4" class="frmBtn"><i class="fa fa-print fa-2x" style="color: black; padding-right:10px;"></i>Print </button>
               <button runat="server" id="Button5" class="frmBtn"><i class="fa fa-window-close fa-2x" style="color: orangered; padding-right:10px;"></i>Close </button>

		</div>
	</div>
</asp:Content>
