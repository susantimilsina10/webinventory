﻿<%@ Page Title="" Language="C#" MasterPageFile="~/navBar.Master" AutoEventWireup="true" CodeBehind="frmOpeningBalEntry.aspx.cs" Inherits="Inventory.WebForm10" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
        <link href="../Content/css/style.css" rel="stylesheet" />
        <link href="https://use.fontawesome.com/releases/v5.0.7/css/all.css" rel="stylesheet">

     <style type="text/css">
        .FixedHeader {
            position: absolute;
            font-weight: bold;
        } 
        
        .labels{
            text-align:right
        }
         </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

		<div class="form-box">
			<h2 align="center">Opening Balance Entry</h2>
			<br/><br/>
			<table>
				<tr>
					<td style="text-align:center;"><asp:Label runat="server" Width="150" > Search Items :</asp:Label></td>
					<td>&nbsp;<asp:DropDownList ID="DropDownList3" runat="server" Width="300" Height="20px">
										<asp:ListItem Text="Volvo"></asp:ListItem>
										<asp:ListItem Text="Mercedes"></asp:ListItem>
										<asp:ListItem Text="Audi"></asp:ListItem>
										<asp:ListItem Text="Saab"></asp:ListItem>
                                    </asp:DropDownList>	</td>
					
					<td>  <asp:Button ID="Button4" runat="server" Text="Submit" BorderStyle="Solid"/>  </td>
				</tr>
			</table>
		<br />
			<asp:Panel ID="groupPanel" runat="server" >
                    <div style="overflow-y: scroll; overflow-x: hidden; height: 300px; width: 100%; background-color:lightgray">
			 <asp:GridView ID="gvDistricts" runat="server" style="height:400px; overflow:scroll" 
                    HeaderStyle-CssClass="FixedHeader" HeaderStyle-BackColor="#9966ff" 
                    AutoGenerateColumns="false" AlternatingRowStyle-BackColor="WhiteSmoke" 
                    ShowHeaderWhenEmpty="True"> 
                             <AlternatingRowStyle BackColor="WhiteSmoke" />
                             <Columns>
                                 <asp:TemplateField HeaderStyle-Width="80px" HeaderStyle-Height="40px" HeaderText="Sr" ItemStyle-Width="80px">
                                     <ItemTemplate>
                                         <asp:Label ID="lblDistID" runat="server" Text='hi'></asp:Label>
                                     </ItemTemplate>
                                     <HeaderStyle Width="82px" />
                                     <ItemStyle Width="82px" />
                                 </asp:TemplateField>
                                 <asp:TemplateField HeaderText="Account Name">
                                     <ItemTemplate>
                                         <asp:Label ID="lblDistName" runat="server" Text='hlo'></asp:Label>
                                     </ItemTemplate>
                                     <HeaderStyle Width="450px" />
                                     <ItemStyle Width="450px" />
                                 </asp:TemplateField>
                                 
                                
                                  <asp:TemplateField HeaderText="Debit Amt">
                                     <ItemTemplate>
                                         <asp:Label ID="lblDistDesc" runat="server" Text='bye'></asp:Label>
                                     </ItemTemplate>
                                     <HeaderStyle Width="122px" />
                                     <ItemStyle Width="122px" />
                                 </asp:TemplateField> 
                                  <asp:TemplateField HeaderText="Credit Amt">
                                     <ItemTemplate>
                                         <asp:Label ID="lblDistDesc" runat="server" Text='bye'></asp:Label>
                                     </ItemTemplate>
                                     <HeaderStyle Width="122px" />
                                     <ItemStyle Width="122px" />
                                 </asp:TemplateField> 
                             </Columns>
                             <HeaderStyle BackColor="YellowGreen" />
                    </asp:GridView>  
                        </div>
            
                </asp:Panel>
                       <asp:Table runat="server" GridLines="Both" Font-Size="Medium" Font-Names="m">
                           <asp:TableHeaderRow Height="30px">
                               <asp:TableHeaderCell HorizontalAlign="Center"  Width="530px">Grand Total</asp:TableHeaderCell>
                              <asp:TableHeaderCell HorizontalAlign="right" Width="120px">0.0</asp:TableHeaderCell>
                               <asp:TableHeaderCell HorizontalAlign="right" Width="120px">0.0</asp:TableHeaderCell>
                           </asp:TableHeaderRow>
                           </asp:Table>
                 <br />
            <asp:Table runat="server"  BorderWidth="0px" Font-Size="Small">
                           <asp:TableHeaderRow>
                              
                               <asp:TableHeaderCell HorizontalAlign="right" Width="650px" >Difference : </asp:TableHeaderCell>
                               <asp:TableHeaderCell HorizontalAlign="right" Width="115px"><asp:TextBox runat="server" Width="115px"></asp:TextBox></asp:TableHeaderCell>
                           </asp:TableHeaderRow>
                           </asp:Table>                   
				<br />
                <asp:Panel runat="server" HorizontalAlign="Center">
                                            <button runat="server" id="Button1" class="buttonsForm"><i class="fa fa-save fa-2x" style="color: Highlight; padding-right:10px;" ></i>Save And Close</button>
                      <button runat="server" id="Button5" class="buttonsForm"><i class="fa fa-sync-alt fa-2x" style="color: green; padding-right:10px;"></i>Refresh </button>
                      <button runat="server" id="Button6" class="buttonsForm"><i class="fa fa-window-close fa-2x" style="color: orangered; padding-right:10px;"></i>Close </button>
                </asp:Panel>
		</div>
</asp:Content>
