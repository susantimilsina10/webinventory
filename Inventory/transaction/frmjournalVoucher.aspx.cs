﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Inventory
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            gvDistricts.DataSource = dt.DefaultView;
            gvDistricts.DataBind();

            //DataTable dtDummyTable = new DataTable();
            //dtDummyTable.Columns.Add(new DataColumn("IndexCol"));
            //for (int i = 0; i < 10; i++)
            //    dtDummyTable.Rows.Add(i + 1);

            //gvDistricts.DataSource = dtDummyTable;
            //gvDistricts.DataBind();
        }


        protected void gvDistricts_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.RowIndex == 0)
                    e.Row.Style.Add("height", "50px");
            }
        }
    }
}