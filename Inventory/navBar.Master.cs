﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Inventory
{
    public partial class navBar : System.Web.UI.MasterPage
    {
     
        protected void Page_Load(object sender, EventArgs e)
        {
           createLedger.ServerClick += new EventHandler(createLedger_Click);
            changeAcc.ServerClick += new EventHandler(changeAcc_Click);
            accRegrouping.ServerClick += new EventHandler(accRegrouping_Click);
            accRegrouping.ServerClick += new EventHandler(accRegrouping_Click);
            changePwd.ServerClick += new EventHandler(changePwd_Click);
            backupData.ServerClick += new EventHandler(backupData_Click);
            journVoucher.ServerClick += new EventHandler(journVoucher_Click);
            paymentVoucher.ServerClick += new EventHandler(paymentVoucher_Click);
            rcptVoucher.ServerClick += new EventHandler(rcptVoucher_Click);
            openingBalanceEntry.ServerClick += new EventHandler(openingBalanceEntry_Click);
            
        }

        protected void createLedger_Click(object sender, EventArgs e)
        {

            Response.Redirect("..\\master\\frmcAccountLedger.aspx");
        } 
        protected void changeAcc_Click(object sender, EventArgs e)
        {
            Response.Redirect("..\\master\\frmchangeAcc.aspx");
        }
        protected void accRegrouping_Click(object sender, EventArgs e)
        {
            Response.Redirect("..\\master\\frmaccRegrouping.aspx");
        }  
        protected void changePwd_Click(object sender, EventArgs e)
        {
            Response.Redirect("..\\systemSec\\frmchangePwd.aspx");
        }
        protected void backupData_Click(object sender, EventArgs e)
        {
            Response.Redirect("..\\systemSec\\frmBackup.aspx");
        } 
        protected void journVoucher_Click(object sender, EventArgs e)
        {
            Response.Redirect("..\\transaction\\frmjournalVoucher.aspx");
        } 
        protected void paymentVoucher_Click(object sender, EventArgs e)
        {
            Response.Redirect("..\\transaction\\frmpaymentVoucher.aspx");
        } 
        protected void rcptVoucher_Click(object sender, EventArgs e)
        {
            Response.Redirect("..\\transaction\\frmReceiptVoucher.aspx");
        }
        protected void openingBalanceEntry_Click(object sender, EventArgs e)
        {
            Response.Redirect("..\\transaction\\frmOpeningBalEntry.aspx");
        }

    }



}