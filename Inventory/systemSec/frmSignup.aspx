﻿<%@ Page Title="" Language="C#" MasterPageFile="~/navBar.Master" AutoEventWireup="true" CodeBehind="frmSignup.aspx.cs" Inherits="Inventory.WebForm7" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
		<link href="../Content/css/style.css" rel="stylesheet" />


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
        <div class="hero">
		<div class="form-box">
			<h1 align="center">Sign Up</h1>
			<br/><br/>
                <asp:Panel ID="groupPanel" runat="server" >
						<div align="center" style="border: 1px solid; padding: 50px; margin-left: 5%; border-color: #c2e59c; margin-right: 5%;">
						<h3>Create an User</h3><br/>
						<table style="width: 80%" cellspacing="20px">
							<tr>
								<td>Username :</td>
								<td>
                                      <asp:TextBox ID="TextBox2" runat="server" Width="80%"></asp:TextBox>	
								</td>
							</tr>
							<tr>
								<td>EmployeeID :</td>
								<td>
						     <asp:TextBox ID="TextBox3" runat="server" Width="80%"></asp:TextBox>	
								</td>
							</tr>
							<tr>
								<td>Employee Name :</td>
								<td>
                                    <asp:TextBox ID="TextBox1" runat="server" Width="80%"></asp:TextBox>
								</td>
							</tr>
							<tr>
								<td>Password :</td>
								<td>
                                    <asp:TextBox ID="TextBox11" runat="server" Width="80%"></asp:TextBox>
								</td>
							</tr>
							<tr>
								<td>Confirm Password :</td>
								<td>
                                    <asp:TextBox ID="TextBox0" runat="server" Width="80%"></asp:TextBox>
								</td>
							</tr>
						</table>
						 
							<br/>
                            <asp:Button ID="gsaveBtn" runat="server" Text="Save" CssClass="submit-btn"/>
							<asp:Button ID="gcancelBtn" runat="server" Text="Cancel" CssClass="submit-btn"/>
							<asp:Button ID="gExitBtn" runat="server" Text="Exit" CssClass="submit-btn"/>
						
                </asp:Panel>
				 <div class="button-wrapper slide-side">
								 <div class="button7div"></div>
									<input class="button7" type="button" name="button7" value="Sleek Side"/>
								 </div>
							</div>

	</div>
</div>

</asp:Content>
