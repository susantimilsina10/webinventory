﻿<%@ Page Title="" Language="C#" MasterPageFile="~/navBar.Master" AutoEventWireup="true" CodeBehind="frmBackup.aspx.cs" Inherits="Inventory.WebForm6" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
	<link href="../Content/css/style.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div class="hero" >
		<div class="form-box" style="height:400px; width:600px; margin:6% auto" >
			<h1 align="center">Backup Data</h1>
			<br/><br/>
                <asp:Panel ID="groupPanel" runat="server" >
						<div align="center" style="height:200px; border: 1px solid; padding: 50px; margin-left: 4%; border-color: #c2e59c; margin-right: 4%;">
					<h3>SQL Server Information</h3><br/>
						<table style="width: 90%" cellspacing="20px">
							<tr>
								<td>Server Name:</td>
								<td>
  <asp:DropDownList ID="DropDownList2" runat="server" Width="255" Enabled="false">
										<asp:ListItem Text="Volvo"></asp:ListItem>
										<asp:ListItem Text="Mercedes"></asp:ListItem>
										<asp:ListItem Text="Audi"></asp:ListItem>
										<asp:ListItem Text="Saab"></asp:ListItem>
                                    </asp:DropDownList>								</td>
							</tr>
						<tr>
								<td>Database:</td>
								<td>
															
  <asp:DropDownList ID="DropDownList1" runat="server" Width="255" Enabled="false">
										<asp:ListItem Text="Volvo"></asp:ListItem>
										<asp:ListItem Text="Mercedes"></asp:ListItem>
										<asp:ListItem Text="Audi"></asp:ListItem>
										<asp:ListItem Text="Saab"></asp:ListItem>
                                    </asp:DropDownList>									</td>
							</tr>

						</table>
					
							<br/>
                            <asp:Button ID="gsaveBtn" runat="server" Text="BackUp" CssClass="submit-btn"/>
											</div>
                </asp:Panel>

	</div>
</div>

</asp:Content>
