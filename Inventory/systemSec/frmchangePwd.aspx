﻿<%@ Page Title="" Language="C#" MasterPageFile="~/navBar.Master" AutoEventWireup="true" CodeBehind="frmchangePwd.aspx.cs" Inherits="Inventory.WebForm5" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
		    <link href="../Content/css/style.css" rel="stylesheet" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="hero" >
		<div class="form-box" style="height:400px; width:600px; margin:6% auto">
			<h1 align="center">Change Password</h1>
			<br/><br/>
                <asp:Panel ID="groupPanel" runat="server" >
						<div align="center" style="height:200px; border: 1px solid; padding: 50px; margin-left: 5%; border-color: #c2e59c; margin-right: 5%;">
					
						<table style="width: 80%" cellspacing="20px">
							<tr>
								<td>Old Password:</td>
								<td>
                                 	    <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
								</td>
							</tr>
							<tr>
								<td></td>
								<td>
								</td>
							</tr>
							<tr>
								<td>New Password:</td>
								<td>
															
									    <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
								</td>
							</tr>
							<tr>
								<td>Confirm Password:</td>
								<td>
                                    <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
								</td>
							</tr>
						</table>
					
							<br/>
                            <asp:Button ID="gsaveBtn" runat="server" Text="Save" CssClass="submit-btn"/>
						
							<asp:Button ID="gExitBtn" runat="server" Text="Exit" CssClass="submit-btn" OnClick="gExitBtn_Click"/>
					</div>
                </asp:Panel>

	</div>
</div>


</asp:Content>
