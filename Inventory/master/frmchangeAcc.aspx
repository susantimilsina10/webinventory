﻿<%@ Page Title="" Language="C#" MasterPageFile="~/navBar.Master" AutoEventWireup="true" CodeBehind="frmchangeAcc.aspx.cs" Inherits="Inventory.WebForm3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<link href="../Content/css/style.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
		<div class="form-box">
			<h1 align="center">Change Account Type</h1>
			<br/><br/>
                <asp:Panel ID="groupPanel" runat="server" >
						<div align="center" style="border: 1px solid; padding: 50px; margin-left: 5%; border-color: #c2e59c; margin-right: 5%;">
						<h3>Select Account</h3><br/>
						<table style="width: 80%" cellspacing="20px">
							<tr>
								<td>Group Code:</td>
								<td>
                                    <asp:TextBox ID="gcodeTxt" runat="server" Text="HX08" ReadOnly="true" Width="255px"></asp:TextBox></td>
							</tr>
							<tr>
								<td>Group Name:</td>
								<td>
										<asp:DropDownList ID="DropDownList1" runat="server" Width="255">
										<asp:ListItem Text="Volvo"></asp:ListItem>
										<asp:ListItem Text="Mercedes"></asp:ListItem>
										<asp:ListItem Text="Audi"></asp:ListItem>
										<asp:ListItem Text="Saab"></asp:ListItem>
                                    </asp:DropDownList>								</td>
							</tr>
							<tr>
								<td>Account Type:</td>
								<td>
                                    <asp:DropDownList ID="dropdownAcc" runat="server" Width="255">
										<asp:ListItem Text="Volvo"></asp:ListItem>
										<asp:ListItem Text="Mercedes"></asp:ListItem>
										<asp:ListItem Text="Audi"></asp:ListItem>
										<asp:ListItem Text="Saab"></asp:ListItem>
                                    </asp:DropDownList>
								</td>
							</tr>
						</table>
						 <br/><br/>
                            <asp:Button ID="gsaveBtn" runat="server" Text="Save" CssClass="submit-btn"/>
							<asp:Button ID="gcancelBtn" runat="server" Text="Cancel" CssClass="submit-btn"/>
							<asp:Button ID="gExitBtn" runat="server" Text="Exit" CssClass="submit-btn"/>
					</div>
                </asp:Panel>

	</div>

</asp:Content>
