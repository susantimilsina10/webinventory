﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Inventory
{
    public partial class WebForm2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //groupAcc.BackColor = System.Drawing.Color.Red;
            groupAcc.Attributes.Add("style", "cursor: pointer;  background: linear-gradient(to right, #c2e59c, #64b3f4);  border: 0;   outline: none;    border - radius: 30px; ");

        }
        protected void groupAcc_Click(object sender, EventArgs e)
        {
            groupPanel.Visible = true;
            groupAcc.Attributes.Add("style", "cursor: pointer;  background: linear-gradient(to right, #c2e59c, #64b3f4);  border: 0;   outline: none;    border - radius: 30px; ");
            childcc.Attributes.Add("style", "background:white");
            childPanel.Visible = false;
            editPanel.Visible = false;
            makingVisible.Visible = true;
        }

        protected void childcc_Click(object sender, EventArgs e) {
          
            groupPanel.Visible = false;
            groupAcc.Attributes.Add("style", "background:white");
            childcc.Attributes.Add("style", "cursor: pointer;  background: linear-gradient(to right, #c2e59c, #64b3f4);  border: 0;   outline: none;    border - radius: 30px; ");

            childPanel.Visible = true;
            editPanel.Visible = false;
            makingVisible.Visible = true;
        }

        protected void ccancelBtn_Click(object sender, EventArgs e)
        {
            groupPanel.Visible = false;
            childPanel.Visible = false;
            editPanel.Visible = true;
            makingVisible.Visible = false;
        }
    }
}