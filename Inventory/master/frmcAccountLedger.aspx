﻿<%@ Page Title="" Language="C#" MasterPageFile="~/navBar.Master" AutoEventWireup="true" CodeBehind="frmcAccountLedger.aspx.cs" Inherits="Inventory.WebForm2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Content/css/style.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

		<div class="form-box">
			<h1 align="center">Create New Account Ledger</h1>
			<br/><br/>
			<asp:Panel runat="server" ID="makingVisible">
						<h3 align="center">Select Group/Child Account</h3>
				<asp:Panel runat="server" CssClass="button-box">
					<asp:Button ID="groupAcc" runat="server" Text="Group Account" CssClass="toggle-btn" OnClick="groupAcc_Click"/>
					<asp:Button ID="childcc" runat="server" Text="Child Account" CssClass="toggle-btn"  onClick="childcc_Click"/>
				</asp:Panel>
			</asp:Panel>
                <asp:Panel ID="groupPanel" runat="server" >
						<div align="center" style="border: 1px solid; padding: 50px; margin-left: 5%; border-color: #c2e59c; margin-right: 5%;">
						<h4>Group Account</h4><br/>
						<table style="width: 80%" cellspacing="20px">
							<tr>
								<td>Group Code:</td>
								<td>
                                    <asp:TextBox ID="gcodeTxt" runat="server" Text="HX08" ReadOnly="true" Width="255px"></asp:TextBox></td>
							</tr>
							<tr>
								<td>Group Name:</td>
								<td>
                                    <asp:TextBox ID="gnameTxt" runat="server" ToolTip="Enter Group Name" Width="255px"></asp:TextBox>
								</td>
							</tr>
							<tr>
								<td>Account Type:</td>
								<td>
                                    <asp:DropDownList ID="dropdownAcc" runat="server" Width="255">
										<asp:ListItem Text="Volvo"></asp:ListItem>
										<asp:ListItem Text="Mercedes"></asp:ListItem>
										<asp:ListItem Text="Audi"></asp:ListItem>
										<asp:ListItem Text="Saab"></asp:ListItem>
                                    </asp:DropDownList>
								</td>
							</tr>
						</table>
						 <br/><br/>
                            <asp:Button ID="gsaveBtn" runat="server" Text="Save" CssClass="submit-btn"/>
							<asp:Button ID="gcancelBtn" runat="server" Text="Cancel" CssClass="submit-btn" OnClick="ccancelBtn_Click"/>
					</div>
                </asp:Panel>

				<asp:Panel ID="childPanel" runat="server" Visible="False">
						<div align="center" style="border: 1px solid; padding: 50px; margin-left: 5%; border-color: #c2e59c; margin-right: 5%;">
						<h4>Child Account</h4><br/>
						<table style="width: 80%" cellspacing="20px">
							<tr>
								<td>Select GroupAc:</td>
								<td>
									 <asp:DropDownList ID="DropDownList1" runat="server" Width="255">
										<asp:ListItem Text="Volvo"></asp:ListItem>
										<asp:ListItem Text="Mercedes"></asp:ListItem>
										<asp:ListItem Text="Audi"></asp:ListItem>
										<asp:ListItem Text="Saab"></asp:ListItem>
                                    </asp:DropDownList>
                                    </td>
							</tr>
							<tr>
								<td>Child Name:</td>
								<td>
                                    <asp:TextBox ID="TextBox2" runat="server" ToolTip="Enter Child Name" Width="255px"></asp:TextBox>
								</td>
							</tr>
							<tr>
								<td>Account Type:</td>
								<td><asp:TextBox ID="TextBox1" runat="server" ToolTip="Enter Account Type" Width="255px"></asp:TextBox> 
								</td>
							</tr>
						</table>
							
                            <asp:Panel ID="Panel1" runat="server" HorizontalAlign="Left" BorderColor="#C2E59C" BorderStyle="Solid" BorderWidth="1" >
								<div style="padding:10px">
									<h4>Break Down of upper Group</h4>
									<asp:Label ID="firstlabel" runat="server" Text="First Label"></asp:Label><br />
									<asp:Label ID="secondlabel" runat="server" Text="Second Label"></asp:Label><br />
									<asp:Label ID="thirdlabel" runat="server" Text="Third Label"></asp:Label><br />
								</div>
								
                            </asp:Panel>
                            <br />
                            <asp:Button ID="csaveBtn" runat="server" Text="Save" CssClass="submit-btn"/>
							<asp:Button ID="ccancelBtn" runat="server" Text="Cancel" CssClass="submit-btn" OnClick="ccancelBtn_Click"/>
					</div>
                </asp:Panel>

			<asp:Panel ID="editPanel" runat="server" Visible="False">
						<div align="center" style="border: 1px solid; padding: 50px; margin-left: 5%; border-color: #c2e59c; margin-right: 5%;">
						<h4>Select Account to Modify</h4><br/>
						<table style="width: 80%" cellspacing="20px">
							<tr>
								<td>Old Account Name:</td>
								<td>
									 <asp:DropDownList ID="DropDownList2" runat="server" Width="255">
										<asp:ListItem Text="Volvo"></asp:ListItem>
										<asp:ListItem Text="Mercedes"></asp:ListItem>
										<asp:ListItem Text="Audi"></asp:ListItem>
										<asp:ListItem Text="Saab"></asp:ListItem>
                                    </asp:DropDownList>
                                    </td>
							</tr>
							<tr>
								<td>New Account Name:</td>
								<td>
                                    <asp:TextBox ID="TextBox3" runat="server" ToolTip="Enter Child Name" Width="255px"></asp:TextBox>
								</td>
							</tr>
							<tr>
								<td>Account Type:</td>
								<td><asp:TextBox ID="TextBox4" runat="server" ToolTip="Enter Account Type" Width="255px"></asp:TextBox> 
								</td>
							</tr>
						</table>
							
                            <asp:Panel ID="Panel3" runat="server" HorizontalAlign="Left" BorderColor="#C2E59C" BorderStyle="Solid" BorderWidth="1" >
								<div style="padding:10px">
									<h4>Break Down of upper Group</h4>
									<asp:Label ID="Label1" runat="server" Text="First Label"></asp:Label><br />
									<asp:Label ID="Label2" runat="server" Text="Second Label"></asp:Label><br />
									<asp:Label ID="Label3" runat="server" Text="Third Label"></asp:Label><br />
								</div>
								
                            </asp:Panel>
                            <br />
						<asp:Button ID="Button2" runat="server" Text="Save" CssClass="submit-btn"/>
							<asp:Button ID="Button3" runat="server" Text="Cancel" CssClass="submit-btn" OnClick="groupAcc_Click"/>
					</div>
                </asp:Panel>
	</div>

</asp:Content>
