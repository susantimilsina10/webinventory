﻿<%@ Page Title="" Language="C#" MasterPageFile="~/navBar.Master" AutoEventWireup="true" CodeBehind="frmaccRegrouping.aspx.cs" Inherits="Inventory.WebForm4" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
	    <link href="../Content/css/style.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
 
		<div class="form-box" style="width: 1000px; height: 600px;">
			<h1 align="center">Changing Account Group</h1>
			<br/><br/>
                <asp:Panel ID="groupPanel" runat="server" >
						<div align="center" style="border: 1px solid; padding: 50px; margin-left: 5%; border-color: #c2e59c; margin-right: 5%;">
						<h3>Select Account</h3><br/>
						<table style="width: 80%" cellspacing="20px">
							<tr>
								<td>Select Account Name:</td>
								<td>
                                   <asp:DropDownList ID="DropDownList2" runat="server" Width="255">
										<asp:ListItem Text="Volvo"></asp:ListItem>
										<asp:ListItem Text="Mercedes"></asp:ListItem>
										<asp:ListItem Text="Audi"></asp:ListItem>
										<asp:ListItem Text="Saab"></asp:ListItem>
                                    </asp:DropDownList>		
								</td>
							</tr>
							<tr>
								<td>Change Group Name:</td>
								<td>
										<asp:DropDownList ID="DropDownList1" runat="server" Width="255" Enabled="False">
										<asp:ListItem Text="Volvo"></asp:ListItem>
										<asp:ListItem Text="Mercedes"></asp:ListItem>
										<asp:ListItem Text="Audi"></asp:ListItem>
										<asp:ListItem Text="Saab"></asp:ListItem>
                                    </asp:DropDownList>								

								</td>
							</tr>
							<tr>
								<td>Account Type:</td>
								<td>
                                    <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
								</td>
							</tr>
						</table>
						    <asp:Panel ID="Panel1" runat="server" HorizontalAlign="Left" BorderColor="#C2E59C" BorderStyle="Solid" BorderWidth="1" >
								<div style="padding:10px">
									<h4>Break Down of upper Group</h4>
									<asp:Label ID="firstlabel" runat="server" Text="First Label"></asp:Label><br />
									<asp:Label ID="secondlabel" runat="server" Text="Second Label"></asp:Label><br />
									<asp:Label ID="thirdlabel" runat="server" Text="Third Label"></asp:Label><br />
								</div>
								
                            </asp:Panel>
							<br/>
                            <asp:Button ID="gsaveBtn" runat="server" Text="Save" CssClass="submit-btn"/>
							<asp:Button ID="gcancelBtn" runat="server" Text="Cancel" CssClass="submit-btn"/>
							<asp:Button ID="gExitBtn" runat="server" Text="Exit" CssClass="submit-btn"/>
					</div>
                </asp:Panel>

	</div>



</asp:Content>
